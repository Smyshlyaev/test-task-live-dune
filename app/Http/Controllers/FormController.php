<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormStoreRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class FormController extends Controller
{
    /**
     * @return View
     */
    public function show(): View
    {
        return view('form');
    }

    /**
     * @param FormStoreRequest $request
     * @return RedirectResponse
     */
    public function store(FormStoreRequest $request): RedirectResponse
    {
        $request->validated();
        return redirect()->route('show_form');
    }
}
