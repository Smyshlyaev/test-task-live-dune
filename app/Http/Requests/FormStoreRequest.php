<?php

namespace App\Http\Requests;

use App\Rules\Diverge;
use Illuminate\Foundation\Http\FormRequest;

class FormStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'current_price' => ['required', 'numeric'],
            'new_price' => ['required', 'numeric', 'bail', new Diverge()],
        ];
    }
}
