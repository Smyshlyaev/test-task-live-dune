<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class Diverge implements Rule, DivergeInterface, DataAwareRule
{
    /**
     * Допустимое отклонение в %
     *
     * @var float
     */
    private $tolerance = 10;

    /**
     * Результат отклонения в %
     *
     * @var float
     */
    private $deviationResult;

    /**
     * Текущая цена
     *
     * @var float
     */
    private $currentPrice;

    /**
     * Новая цена
     *
     * @var float
     */
    private $newPrice;

    /**
     * Получаем данные для валидации
     *
     * @param $data
     * @return $this|Diverge
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * В месте мы валидируем цены по отклонению
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->diffPrice($this->data['new_price'], $this->data['current_price']);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The deviation is too big';
    }

    /**
     * Отклонение цены не должно быть больше допустимого значения (%)
     * Указанные ниже переменные избыточны, т.к. у Laravel есть штатный способ получения этих данных.
     * Оставляю их только потому, что в условиях задачи нужно реализовывать интерфейс c этими переменными
     *
     * @param float $newPrice новая цена, которую будем проверять на отклонение.
     * @param float $currentPrice текущая цена.
     * @return bool
     */
    public function diffPrice(float $newPrice, float $currentPrice): bool
    {
        $this->newPrice = $newPrice;
        $this->currentPrice = $currentPrice;
        $tolerance = $this->tolerance;

        return $this->getDeviation() <= $tolerance;
    }

    /**
     * Результат отклонения в %
     * В случае, если результат будет больше, чем допустимое отклонение, то метод
     * возвращает false. В любых других случаях возвращается true.
     *
     * @return float
     */
    public function getDeviation(): float
    {

        // По условию задачи в классе должно быть приватное свойство с результатом отклонения
        if ($this->currentPrice === 0) {
            $this->deviationResult = 100;
        } else {
            $this->deviationResult = abs((($this->newPrice * 100) / $this->currentPrice) - 100);
        }

        return $this->deviationResult;
    }
}
