<form action="{{route('store_form')}}">
    @csrf
    <input type="text" name="current_price" placeholder="Current price">
    <input type="text" name="new_price" placeholder="New price">
    @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
    @endforeach
    <button type="submit">Submit</button>
</form>
