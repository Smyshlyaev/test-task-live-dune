<?php

namespace Tests\Unit;

use App\Rules\Diverge;
use PHPUnit\Framework\TestCase;

class DivergeTest extends TestCase
{
    /**
     * @var
     */
    protected $rule;

    protected function setUp(): void
    {
        $this->rule = new Diverge();
    }

    /**
     * @dataProvider additionProvider
     */
    public function dataProvider()
    {
        return [
            ['current_price' => 100, 'new_price' => 109, true, 9.0],
            ['current_price' => 100, 'new_price' => 111, false, 11.0],
            ['current_price' => 100, 'new_price' => 0, false, 100.0],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testDivergeLogic($currentPrice, $newPrice, $diffPrice, $deviation)
    {
        $this->rule->setData(['current_price' => $currentPrice, 'new_price' => $newPrice]);
        $resultDiffPrice = $this->rule->diffPrice($newPrice,$currentPrice);
        $resultGetDeviation = $this->rule->getDeviation();
        $this->assertSame($resultDiffPrice, $diffPrice);
        $this->assertSame($resultGetDeviation, $deviation);
        $this->assertSame($diffPrice, $this->rule->passes('new_price', $newPrice));
    }
}
